import cv2
import numpy as np
import imutils
def cropblack(image):
  img = image
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
  contours = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
  cnt = contours[0]
  print(cnt)
  x, y, w, h = cv2.boundingRect(np.array(cnt))
  crop = img[y:y+h, x:x+w]
  return crop
cap = cv2.VideoCapture('./IMG_7951.MOV')
count = 0
img_count = 0
print(cap.isOpened());
while (cap.isOpened()):
  ret, frame = cap.read()
  if ret == True and count % 30 == 0:
    frame = imutils.rotate(frame, 270) 
    frame = cropblack(frame)
    count += 1
    cv2.imwrite("./out_7951/a" + str(img_count) + ".jpg", frame)
    img_count += 1
  if cv2.waitKey(1) & 0xFF == ord('q'):
      break
  elif ret == True and count % 30 !=  0:
    count += 1
  else:
    break
cap.release()
cv2.destroyAllWindows()
