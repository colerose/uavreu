import os
import random
name = "AR"
date = "june_19_2019"
jform = ".jpg"
video_photos = 'june_19_AR/'
count_train = 0
count_test = 0
for filepathphotos in os.listdir('./' + video_photos):
	print('video: ', filepathphotos)
	d = os.listdir(video_photos + filepathphotos)
	random.shuffle(d)
	random.shuffle(d)
	random.shuffle(d)
	random.shuffle(d)
	if os.path.isdir(str(name)) == False:
		os.mkdir(str(name))
	trainpath = "./" + name + "/" + name + "_train/"
	testpath = "./" + name + "/" + name + "_test/"
	if os.path.isdir(trainpath) == False:
		os.mkdir(trainpath)
	if os.path.isdir(testpath) == False:
		os.mkdir(testpath)
	f = d
	s = random.sample(f, int(.2*len(f)))
	e = []
	fullpath = './' + video_photos + '/' + filepathphotos +'/'
	for x in f:
	  if x not in s:
	    e.append(x)
	for x in s:
	  form = str(date) + "_" + str(name) + "_test_" + str(count_test) + str(jform)
	  #print(form)
	  print(fullpath + x)
	  os.rename('./' + video_photos + '/' + filepathphotos +'/' + x, testpath + form) 
	  count_test += 1
	for x in e:
	  form = str(date) + "_" + str(name) + "_train_" + str(count_train) + str(jform)
	  #print(form)
	  os.rename(fullpath + x, trainpath + form)
	  count_train += 1
