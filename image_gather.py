from google_images_download import google_images_download  
import os
import sys
#importing the library
def gather(keyword, limit, count, tag): 
    response = google_images_download.googleimagesdownload()   #class instantiation
    arguments = {"keywords":keyword,"limit":limit,"print_urls":True, "chromedriver":'/home/uav/uavreu/chromedriver'}   #creating list of arguments
    paths = response.download(arguments)   #passing the arguments to the function
    print(paths)   #printing absolute paths of the downloaded image
    image_path = os.path.abspath("./downloads/" + keyword + "/")   #gets absolute path of imported images
    count = 0
    for x in os.listdir(image_path):
      if '.jpg' not in x:
        os.remove(image_path + '/' + x)   #if the image is not a jpg, remove it
      else:
        os.rename(image_path + '/' + x, image_path + '/' + keyword + str(count) + '.jpg')   
        count += 1    #rename all files with a consistent pattern
gather(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]) #takes in command line arguments
