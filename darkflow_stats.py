import pandas as pd
import os
import json
test_results_dir = "./drone_test/out"
json_files = [x for x in os.listdir(test_results_dir) if "json" in x]
no_predictions = 0
total_files = len(json_files)
num_ar_predicted = 0
num_bebop_predicted = 0
num_mavic_predicted = 0
total_ar_confidence = 0
total_bebop_confidence = 0
total_mavic_confidence = 0
double_counted_bebop = 0
double_counted_ar = 0
double_counted_mavic = 0
triple_preds = 0
for json_file in json_files:
	json_data = json.load(open(os.path.join(test_results_dir,json_file)))
	if len(json_data) == 0:
		no_predictions = 0
	local_count_ar = 0
	local_count_bebop = 0
	local_count_mavic = 0
	for pred in json_data:
		if pred['label'] == 'AR Parrot Drone':
			num_ar_predicted += 1
			total_ar_confidence += pred['confidence']
			local_count_ar +=1
		elif pred['label'] == 'Parrot Bebop Drone':
			num_bebop_predicted +=1
			total_bebop_confidence += pred['confidence']
			local_count_bebop +=1 
		elif pred['label'] == 'DJI Mavic Air Drone':
			num_mavic_predicted +=1
			total_mavic_confidence += pred['confidence']
			local_count_mavic +=1 
	if local_count_ar >= 2:
		double_counted_ar += 1
	elif local_count_bebop >= 2:
		double_counted_bebop +=1
	elif local_count_mavic >= 2:
		double_counted_mavic +=1
	if (local_count_ar > 2):
		print("num ar count in ", json_file, " ", local_count_ar)
		triple_preds += 1
	elif local_count_bebop > 2:
		print("num bebop count in ", json_file, " ", local_count_bebop)
		triple_preds += 1
	elif local_count_mavic > 2:
		print("num mavic count in ", json_file, " ", local_count_bebop)
		triple_preds += 1
total_preds = num_ar_predicted + num_bebop_predicted + num_mavic_predicted
avg_ar_conf = total_ar_confidence/num_ar_predicted
avg_bebop_conf = total_bebop_confidence/num_bebop_predicted
avg_mavic_conf = total_mavic_confidence/num_mavic_predicted
total_over_counted = double_counted_ar + double_counted_bebop
avg_overall_conf = (total_bebop_confidence + total_ar_confidence + total_mavic_confidence)/total_preds
relaxed_num_preds = total_preds - (total_over_counted - triple_preds) - (2*triple_preds)
result_table_contents = [(avg_ar_conf, avg_bebop_conf, avg_mavic_conf, avg_overall_conf), (num_ar_predicted, num_bebop_predicted, num_mavic_predicted, total_preds), 
	(double_counted_ar, double_counted_bebop, double_counted_mavic, (total_over_counted)), ("N/A", "N/A", "N/A", (relaxed_num_preds/total_files))]
dfObj = pd.DataFrame(result_table_contents, columns = ['AR' , 'Bebop', 'Mavic', 'Total'], index=['Average Confidence', "Num Predictions", "Over Counted Predictions", "Average Prediction Per Photo"])
print(" ")
print("------------------------------------")
print("Total test photos: ", total_files)
print(dfObj)